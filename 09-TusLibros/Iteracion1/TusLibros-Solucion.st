!classDefinition: #CarritoTest category: 'TusLibros-Solucion'!
TestCase subclass: #CarritoTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros-Solucion'!

!CarritoTest methodsFor: 'as yet unclassified' stamp: 'TI 6/8/2023 13:09:44'!
test01CreateACartCreatesItEmpty

	| carrito |
	carrito := Carrito with: OrderedCollection new.
	self assert: carrito isEmpty equals: true.! !

!CarritoTest methodsFor: 'as yet unclassified' stamp: 'TI 6/8/2023 13:09:05'!
test02AddValidProductToCartAddsIt
	| carrito store |
	carrito := Carrito with: OrderedCollection new.
	store := Store with: (OrderedCollection with: 'libro1').
	
	store addCarrito: carrito.
	store addToCart: carrito product: 'libro1' quantity: 1.
	self assert: (carrito containsProduct: 'libro1') equals: true.! !

!CarritoTest methodsFor: 'as yet unclassified' stamp: 'TI 6/7/2023 13:29:25'!
test03AddValidProductToCartManyTimesAddsTheProductThatAmountOfTimes
	| carrito store |
	carrito := Carrito with: OrderedCollection new.
	store := Store with: (OrderedCollection with: 'libro1').
	
	store addCarrito: carrito.
	store addToCart: carrito product: 'libro1' quantity: 5.
	self assert: (carrito containsProduct: 'libro1' times: 5) equals: true.! !

!CarritoTest methodsFor: 'as yet unclassified' stamp: 'TI 6/8/2023 13:39:55'!
test04AddInvalidProductToCartRaisesError
	| carrito store |
	carrito := Carrito with: OrderedCollection new.
	store := Store with: (OrderedCollection with: 'libro1').
	
	store addCarrito: carrito.
	
	self 
		should: [ store addToCart: carrito product: 'mermelada de frutilla' quantity: 1.]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText equals: store invalidISBN.
		]! !

!CarritoTest methodsFor: 'as yet unclassified' stamp: 'TI 6/8/2023 13:41:39'!
test05AddProductNegativeAmountOfTimesRaisesError
	| carrito store |
	carrito := Carrito with: OrderedCollection new.
	store := Store with: (OrderedCollection with: 'libro1').
	
	store addCarrito: carrito.
	
	
	self 
		should: [ store addToCart: carrito product: 'libro1' quantity: -1.]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText equals: carrito errorCantAddNegativeAmountOfProducts.
		]! !

!CarritoTest methodsFor: 'as yet unclassified' stamp: 'MF 6/8/2023 12:07:15'!
test06ListCartItemsReturnsItemsAndQuantityOfItemInCart

	| carrito store result |
	carrito := Carrito with: OrderedCollection new.
	store := Store with: (OrderedCollection with: 'libro1' with:'libro2').
	
	store addCarrito: carrito.
	store addToCart: carrito product: 'libro1' quantity: 2.
	store addToCart: carrito product: 'libro2' quantity: 5.
	result := Dictionary new.
	result add: 'libro1' -> 2.
	result add: 'libro2' -> 5.
	
	self assert: carrito listItems equals: result.! !


!classDefinition: #Carrito category: 'TusLibros-Solucion'!
Object subclass: #Carrito
	instanceVariableNames: 'conjuntoDeProductos productos'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros-Solucion'!

!Carrito methodsFor: 'initialization' stamp: 'TI 6/7/2023 13:03:41'!
initializeWith: unConjuntoDeProductos 
	
	productos := unConjuntoDeProductos.! !


!Carrito methodsFor: 'testing' stamp: 'TI 6/7/2023 13:12:15'!
containsProduct: aProduct 
	
	^productos includes: aProduct.! !

!Carrito methodsFor: 'testing' stamp: 'TI 6/7/2023 13:33:54'!
containsProduct: aProduct times: aQuantity 
	
	| appearances |
	appearances := 0.
	productos do:[:product |
		product = aProduct ifTrue:
		[appearances := appearances + 1.]
	].
	^(appearances = aQuantity)! !

!Carrito methodsFor: 'testing' stamp: 'TI 6/8/2023 13:10:10'!
isEmpty
	^(productos size = 0).! !


!Carrito methodsFor: 'actions' stamp: 'TI 6/8/2023 13:40:48'!
addToCart: aProduct quantity: anAmount
	
	| i |
	
	anAmount < 0 ifTrue:  [		
		self error: self errorCantAddNegativeAmountOfProducts.		
		].

	i:=0.
	[i<anAmount]whileTrue:[
		productos add: aProduct.
		i := i + 1.
		].! !


!Carrito methodsFor: 'errors' stamp: 'TI 6/8/2023 13:41:18'!
errorCantAddNegativeAmountOfProducts
	^'No se puede añadir una cantidad de productos negativa.'! !


!Carrito methodsFor: 'about' stamp: 'MF 6/8/2023 12:01:51'!
listItems
	
	|itemsList found |
	
	itemsList := Dictionary new.
	
	
	productos do: [ :aProduct |
		found := false.	
		itemsList  keysDo: [ :aProductInCart |
			aProduct = aProductInCart value ifTrue:[
				itemsList  at: aProductInCart put: ((itemsList at: aProductInCart) + 1).
				found := true.
				].
			].
			found ifFalse:[
				itemsList  add: aProduct -> 1.
				].			
		].
	
	^itemsList 
	
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Carrito class' category: 'TusLibros-Solucion'!
Carrito class
	instanceVariableNames: ''!

!Carrito class methodsFor: 'initialization' stamp: 'TI 6/7/2023 13:03:20'!
with: unConjuntoDeProductos 
	
	^self new initializeWith: unConjuntoDeProductos.
	! !


!Carrito class methodsFor: 'as yet unclassified' stamp: 'TI 6/7/2023 13:21:05'!
with: aRenameMe1 in: store 
	self shouldBeImplemented.! !


!classDefinition: #Store category: 'TusLibros-Solucion'!
Object subclass: #Store
	instanceVariableNames: 'productsCollection productsOnSale carritos'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros-Solucion'!

!Store methodsFor: 'Initialization' stamp: 'TI 6/7/2023 13:25:26'!
initializeWith: aProductsCollection 

	productsOnSale := aProductsCollection.
	carritos := OrderedCollection new.! !


!Store methodsFor: 'actions' stamp: 'TI 6/7/2023 13:24:43'!
addCarrito: aCarrito 
	carritos add: aCarrito! !

!Store methodsFor: 'actions' stamp: 'TI 6/8/2023 13:36:50'!
addToCart: aCarrito product: aProduct quantity: aQuantity
	
	(productsOnSale includes: aProduct) 
	ifTrue: [aCarrito addToCart: aProduct quantity: aQuantity.]
	ifFalse: [self error: self invalidISBN.].
	
	! !


!Store methodsFor: 'errors' stamp: 'TI 6/8/2023 13:38:20'!
invalidISBN
	^'Invalid ISBN.'! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Store class' category: 'TusLibros-Solucion'!
Store class
	instanceVariableNames: ''!

!Store class methodsFor: 'instance creation' stamp: 'TI 6/7/2023 13:08:51'!
with: aProductsCollection 
	^self new initializeWith: aProductsCollection.! !
