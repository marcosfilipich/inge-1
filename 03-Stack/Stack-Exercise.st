!classDefinition: #OOStackTest category: 'Stack-Exercise'!
TestCase subclass: #OOStackTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStackTest methodsFor: 'test' stamp: 'HernanWilkinson 5/7/2012 11:30'!
test01StackShouldBeEmptyWhenCreated

	| stack |
	
	stack := OOStack new.
	
	self assert: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:29:55'!
test02PushAddElementsToTheStack

	| stack |
	
	stack := OOStack new.
	stack push: 'something'.
	
	self deny: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:01'!
test03PopRemovesElementsFromTheStack

	| stack |
	
	stack := OOStack new.
	stack push: 'something'.
	stack pop.
	
	self assert: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:09'!
test04PopReturnsLastPushedObject

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'something'.
	stack push: pushedObject.
	
	self assert: stack pop = pushedObject! !

!OOStackTest methodsFor: 'test' stamp: 'NR 9/16/2021 17:40:17'!
test05StackBehavesLIFO

	| stack firstPushedObject secondPushedObject |
	
	stack := OOStack new.
	firstPushedObject := 'firstSomething'.
	secondPushedObject := 'secondSomething'.
	
	stack push: firstPushedObject.
	stack push: secondPushedObject.
	
	self assert: stack pop = secondPushedObject.
	self assert: stack pop = firstPushedObject.
	self assert: stack isEmpty 
	! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:20'!
test06TopReturnsLastPushedObject

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'something'.
	
	stack push: pushedObject.
	
	self assert: stack top = pushedObject.
	! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:24'!
test07TopDoesNotRemoveObjectFromStack

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'something'.
	
	stack push: pushedObject.
	
	self assert: stack size = 1.
	stack top.
	self assert: stack size = 1.
	! !

!OOStackTest methodsFor: 'test' stamp: 'HAW 4/14/2017 22:48:26'!
test08CanNotPopWhenThereAreNoObjectsInTheStack

	| stack  |
	
	stack := OOStack new.
	self
		should: [ stack pop ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:31'!
test09CanNotPopWhenThereAreNoObjectsInTheStackAndTheStackHadObjects

	| stack  |
	
	stack := OOStack new.
	stack push: 'something'.
	stack pop.
	
	self
		should: [ stack pop ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !

!OOStackTest methodsFor: 'test' stamp: 'HAW 4/14/2017 22:48:44'!
test10CanNotTopWhenThereAreNoObjectsInTheStack

	| stack  |
	
	stack := OOStack new.
	self
		should: [ stack top ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !


!classDefinition: #SentenceFinderByPrefixTest category: 'Stack-Exercise'!
TestCase subclass: #SentenceFinderByPrefixTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!SentenceFinderByPrefixTest methodsFor: 'test' stamp: 'MF 4/24/2023 10:26:00'!
test01ASentenceFinderByPrefixReturnsEmptyListWhenSentenceNotMatch

	|aStack aPrefix aSolution aSentenceFinder |

	aSentenceFinder := SentenceFinderByPrefix new.
	
	aPrefix  := 'Mar'.
	
	aStack := OOStack  new.
	
	aStack push: 'Hola, como estas?'.
	aStack push: 'Me llamo Marcos'.
	aStack push: 'La paso muy bien codeando en SmallTalk'.
	
	aSolution := aSentenceFinder  findSentenceIn: aStack by: aPrefix.
	
	 self assert: aSolution = OrderedCollection new. 
	 ! !

!SentenceFinderByPrefixTest methodsFor: 'test' stamp: 'MF 4/24/2023 10:27:49'!
test02ASentenceFinderByPrefixReturnsACollectionWithOneSentenceWhenMatched

	|aStack aPrefix aSolution anOrderedCollection aSentenceFinder |

	aSentenceFinder := SentenceFinderByPrefix new.
	
	aPrefix  := 'Mar'.
	
	aStack := OOStack  new.
	
	aStack push: 'Hola, como estas?'.
	aStack push: 'Me llamo Marcos'.
	aStack push: 'La paso muy bien codeando en SmallTalk'.
	aStack push: 'Martes es mi dia favorito de la semana'.
	
	aSolution := aSentenceFinder  findSentenceIn: aStack by: aPrefix.
	
	anOrderedCollection  := OrderedCollection new.
	anOrderedCollection add: 'Martes es mi dia favorito de la semana'.
	
	 self assert: aSolution = anOrderedCollection . 
	 ! !

!SentenceFinderByPrefixTest methodsFor: 'test' stamp: 'MF 4/24/2023 10:31:13'!
test03ASentenceFinderByPrefixReturnsACollectionWithMultipleSentencesWhenMatched

	|aStack aPrefix aSolution anOrderedCollection aSentenceFinder |
	
	aPrefix  := 'Mar'.
	
	aStack := OOStack  new.
	
	aSentenceFinder := SentenceFinderByPrefix  new.
	
	aStack push: 'Hola, como estas?'.
	aStack push: 'Marcos es mi nombre'.
	aStack push: 'La paso muy bien codeando en SmallTalk'.
	aStack push: 'Martes es mi dia favorito de la semana'.
	aStack push: 'Maratea me cae mal'. 
	
	aSolution := aSentenceFinder  findSentenceIn: aStack by: aPrefix.
	
	anOrderedCollection  := OrderedCollection new.	
	
	anOrderedCollection add: 'Maratea me cae mal'.
	anOrderedCollection add: 'Martes es mi dia favorito de la semana'.
	anOrderedCollection add: 'Marcos es mi nombre'.
	
	 self assert: aSolution = anOrderedCollection . 
	 ! !

!SentenceFinderByPrefixTest methodsFor: 'test' stamp: 'MF 4/24/2023 10:28:56'!
test04ASentenceFinderByPrefixReturnsAnEmptyCollectionWhenStackIsEmpty

	|aStack aPrefix aSolution anOrderedCollection aSentenceFinder |
	
	aPrefix  := 'Mar'.
	
	aStack := OOStack  new.
	
	aSentenceFinder := SentenceFinderByPrefix  new.
	
	aSolution := aSentenceFinder  findSentenceIn: aStack by: aPrefix.
	
	anOrderedCollection  := OrderedCollection new.
	
	 self assert: aSolution = anOrderedCollection . 
	 ! !

!SentenceFinderByPrefixTest methodsFor: 'test' stamp: 'MF 4/24/2023 10:29:22'!
test05ASentenceFinderByPrefixIsCaseSensitive

	|aStack aPrefix aSolution anOrderedCollection aSentenceFinder |
	
	aPrefix  := 'Mar'.
	
	aStack := OOStack  new.
	
	aSentenceFinder := SentenceFinderByPrefix  new.
	
	aStack push: 'marcos y tobias son unos capos.'.
	
	aSolution := aSentenceFinder  findSentenceIn: aStack by: aPrefix.
	
	anOrderedCollection  := OrderedCollection new.
	
	 self assert: aSolution = anOrderedCollection . 
	 ! !

!SentenceFinderByPrefixTest methodsFor: 'test' stamp: 'MF 4/24/2023 10:44:06'!
test06ASentenceFinderByPrefixShouldReturnErrorWhenPrefixIsMoreThanOneWord

	|aStack aSpacedPrefix aSentenceFinder |
	
	aSpacedPrefix  := 'marcos y'.
	
	aStack := OOStack  new.
	
	aSentenceFinder := SentenceFinderByPrefix  new.
	
	aStack push: 'marcos y tobias son unos capos.'.
	
	self
        should: [ aSentenceFinder  findSentenceIn: aStack by: aSpacedPrefix.]
        raise: Error - MessageNotUnderstood 
        withExceptionDo: [ :anError |
            self assert: anError messageText = SentenceFinderByPrefix sentenceFinderPrefixWithSpacesErrorDescription ]
	 ! !

!SentenceFinderByPrefixTest methodsFor: 'test' stamp: 'MF 4/24/2023 10:37:14'!
test07ASentenceFinderByPrefixDoesntModifyTheStack

	|aStack originalStack aPrefix aSolution anOrderedCollection aSentenceFinder |
	
	aPrefix  := 'Mar'.
	
	aStack := OOStack  new.
	
	aSentenceFinder := SentenceFinderByPrefix  new.
	
	aStack push: 'Hola, como estas?'.
	aStack push: 'Marcos es mi nombre'.
	aStack push: 'La paso muy bien codeando en SmallTalk'.
	aStack push: 'Martes es mi dia favorito de la semana'.
	aStack push: 'Maratea me cae mal'. 
	
	originalStack := aStack .
	
	aSolution := aSentenceFinder  findSentenceIn: aStack by: aPrefix.
	
	anOrderedCollection  := OrderedCollection new.	
	
	anOrderedCollection add: 'Maratea me cae mal'.
	anOrderedCollection add: 'Martes es mi dia favorito de la semana'.
	anOrderedCollection add: 'Marcos es mi nombre'.
	
	self assert: originalStack = aStack.
	 ! !

!SentenceFinderByPrefixTest methodsFor: 'test' stamp: 'MF 4/24/2023 10:44:29'!
test08ASentenceFinderByPrefixShouldReturnErrorWhenPrefixIsEmpty

	|aStack anEmptyPrefix aSentenceFinder |
	
	anEmptyPrefix  := ''.
	
	aStack := OOStack  new.
	
	aSentenceFinder := SentenceFinderByPrefix  new.
	
	aStack push: 'marcos y tobias son unos capos.'.
	
	self
        should: [ aSentenceFinder  findSentenceIn: aStack by: anEmptyPrefix.]
        raise: Error - MessageNotUnderstood 
        withExceptionDo: [ :anError |
            self assert: anError messageText = SentenceFinderByPrefix sentenceFinderEmptyPrefixErrorDescription ]
	 ! !


!classDefinition: #OOStack category: 'Stack-Exercise'!
Object subclass: #OOStack
	instanceVariableNames: 'top size'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStack methodsFor: 'nil' stamp: 'TI 4/22/2023 17:46:16'!
initialize
	| base |
	base := Base new.
	top := base.
	size := 0.
	^self.
	! !


!OOStack methodsFor: 'private' stamp: 'MF 4/23/2023 15:50:26'!
decrementSizeInOne

	^size := size -1.
	! !

!OOStack methodsFor: 'private' stamp: 'MF 4/23/2023 15:35:51'!
setNewTop: anElement.

	top := anElement.
	! !


!OOStack methodsFor: 'operations' stamp: 'TI 4/22/2023 17:49:24'!
isEmpty

	^size = 0.
	! !

!OOStack methodsFor: 'operations' stamp: 'MF 4/24/2023 09:31:15'!
pop

	^ top popElement: self! !

!OOStack methodsFor: 'operations' stamp: 'TI 4/22/2023 18:39:41'!
push: anElement

	| element |
	element := Element new initializeWith: anElement.
	element setPrevious: top.
	top := element.
	size := size + 1.
	^self.
	! !

!OOStack methodsFor: 'operations' stamp: 'TI 4/22/2023 18:54:18'!
size

	^size.
	! !

!OOStack methodsFor: 'operations' stamp: 'MF 4/23/2023 13:12:27'!
top

	^ top topElement.
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'OOStack class' category: 'Stack-Exercise'!
OOStack class
	instanceVariableNames: ''!

!OOStack class methodsFor: 'error descriptions' stamp: 'NR 9/16/2021 17:39:43'!
stackEmptyErrorDescription
	
	^ 'stack is empty!!!!!!'! !


!classDefinition: #Base category: 'Stack-Exercise'!
OOStack subclass: #Base
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!Base methodsFor: 'as yet unclassified' stamp: 'TI 4/22/2023 17:46:01'!
initialize! !

!Base methodsFor: 'as yet unclassified' stamp: 'MF 4/23/2023 15:38:10'!
popElement: anElement

^self error: OOStack stackEmptyErrorDescription! !

!Base methodsFor: 'as yet unclassified' stamp: 'MF 4/23/2023 13:11:06'!
topElement

^self error: OOStack stackEmptyErrorDescription! !


!classDefinition: #Element category: 'Stack-Exercise'!
OOStack subclass: #Element
	instanceVariableNames: 'value previous'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!Element methodsFor: 'as yet unclassified' stamp: 'TI 4/22/2023 18:44:10'!
initializeWith: anElement
	value := anElement.
	^self.! !

!Element methodsFor: 'as yet unclassified' stamp: 'MF 4/23/2023 15:36:43'!
popElement:aStack

	| poppedElement newTop|
	
	newTop :=  self previous.
	poppedElement := self value.
	aStack setNewTop: newTop.
	aStack decrementSizeInOne.
	
	^poppedElement.! !

!Element methodsFor: 'as yet unclassified' stamp: 'TI 4/22/2023 18:16:22'!
previous

	^previous.! !

!Element methodsFor: 'as yet unclassified' stamp: 'TI 4/22/2023 18:39:09'!
setPrevious: anElement

	previous := anElement.! !

!Element methodsFor: 'as yet unclassified' stamp: 'MF 4/23/2023 14:50:44'!
size

	^size.! !

!Element methodsFor: 'as yet unclassified' stamp: 'MF 4/23/2023 14:51:46'!
top

	^top.! !

!Element methodsFor: 'as yet unclassified' stamp: 'MF 4/23/2023 13:11:48'!
topElement

	^value.! !

!Element methodsFor: 'as yet unclassified' stamp: 'TI 4/22/2023 18:18:22'!
value

	^value.! !


!classDefinition: #SentenceFinderByPrefix category: 'Stack-Exercise'!
Object subclass: #SentenceFinderByPrefix
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!SentenceFinderByPrefix methodsFor: 'aux' stamp: 'TI 4/24/2023 12:02:09'!
checkIfPrefixIsCorrect: aPrefix

	(aPrefix includesSubString: ' ') ifTrue: [^self error: SentenceFinderByPrefix sentenceFinderPrefixWithSpacesErrorDescription.].
	
	(aPrefix isEmpty) ifTrue: [^self error: SentenceFinderByPrefix sentenceFinderEmptyPrefixErrorDescription .].! !

!SentenceFinderByPrefix methodsFor: 'aux' stamp: 'MF 4/24/2023 10:27:00'!
isThePrefix: aPrefix of: aSentence		
	
	|lengthOfPrefix|
	
	lengthOfPrefix  := aPrefix size.
	
	^ aPrefix = (aSentence copyFrom:1 to: lengthOfPrefix). ! !

!SentenceFinderByPrefix methodsFor: 'aux' stamp: 'MF 4/24/2023 10:26:24'!
transferElementsFrom: aStack to: anotherStack

	|anElement|
	
	[aStack isEmpty] whileFalse: [		
		anElement := 	aStack pop.
		anotherStack  push: anElement		
		 .].! !


!SentenceFinderByPrefix methodsFor: 'operations' stamp: 'TI 4/24/2023 11:09:52'!
findSentenceIn: aStack by: aPrefix

	|temporaryStack solution topElement |
	
	temporaryStack  := OOStack new.
	solution := OrderedCollection new.
	
	self checkIfPrefixIsCorrect: aPrefix.
	
	[aStack isEmpty] whileFalse: [
		
		topElement := aStack top.
		(self isThePrefix: aPrefix of: topElement)	 ifTrue: [ 
			solution add: topElement.
		].
		temporaryStack push: topElement.
		aStack pop.		
		].
	
	self transferElementsFrom: temporaryStack to: aStack.
		
	^ solution.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'SentenceFinderByPrefix class' category: 'Stack-Exercise'!
SentenceFinderByPrefix class
	instanceVariableNames: ''!

!SentenceFinderByPrefix class methodsFor: 'error messages' stamp: 'MF 4/24/2023 10:48:20'!
sentenceFinderEmptyPrefixErrorDescription

	^'El prefijo mandado por parametro esta vacio.'.! !

!SentenceFinderByPrefix class methodsFor: 'error messages' stamp: 'MF 4/24/2023 10:47:28'!
sentenceFinderPrefixWithSpacesErrorDescription

	^'El prefijo mandado por parametro tiene espacios.'! !
