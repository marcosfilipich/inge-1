!classDefinition: #CantSuspend category: 'CodigoRepetido-Ejercicio'!
Error subclass: #CantSuspend
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!


!classDefinition: #NotFound category: 'CodigoRepetido-Ejercicio'!
Error subclass: #NotFound
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!


!classDefinition: #CustomerBookTest category: 'CodigoRepetido-Ejercicio'!
TestCase subclass: #CustomerBookTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!

!CustomerBookTest methodsFor: 'auxiliary' stamp: 'ti 4/10/2023 21:39:15'!
actionTakeLessThanCantTime: codigo cantTime: cantTime

	  
	| millisecondsAfterRunning millisecondsBeforeRunning |
	millisecondsBeforeRunning := Time millisecondClockValue * millisecond.
	codigo.
	millisecondsAfterRunning := Time millisecondClockValue * millisecond.
	
	self assert: (millisecondsAfterRunning-millisecondsBeforeRunning) < (cantTime * millisecond)
	
! !

!CustomerBookTest methodsFor: 'auxiliary' stamp: 'TI 4/11/2023 19:47:02'!
addAndSuspendCustomer: customerName

	| customerBook customer |
	customerBook := CustomerBook new.
	customer := customerName.
	
	customerBook addCustomerNamed: customer.
	customerBook suspendCustomerNamed: customer.
	^customerBook! !

!CustomerBookTest methodsFor: 'auxiliary' stamp: 'TI 4/11/2023 19:46:25'!
addCustomer: nameCustomer
	
	| customerBook customer |
			
	customerBook := CustomerBook new.
	customer := nameCustomer.
	customerBook addCustomerNamed: customer.
	
	^customerBook.! !

!CustomerBookTest methodsFor: 'auxiliary' stamp: 'TI 4/11/2023 19:35:06'!
checkQuantityOfDifferentTypeOfCustomers: customerBook numberOfActiveCustomers: numberOfActiveCustomers numberOfSuspendedCustomers: numberOfSuspendedCustomers numberOfCustomers: numberOfCustomers
	
	self assert: numberOfActiveCustomers equals: customerBook numberOfActiveCustomers.
	self assert: numberOfSuspendedCustomers equals: customerBook numberOfSuspendedCustomers.
	self assert: numberOfCustomers equals: customerBook numberOfCustomers.! !

!CustomerBookTest methodsFor: 'auxiliary' stamp: 'ti 4/10/2023 21:54:45'!
invalidCodeThatMustFail: valueIfFails failCondition: failCondition errorCode: errorCode

	[ valueIfFails value.
	self fail ]
		on: failCondition 
		do: [errorCode ]
! !


!CustomerBookTest methodsFor: 'testing' stamp: 'ti 4/10/2023 21:39:50'!
test01AddingCustomerShouldNotTakeMoreThan50Milliseconds

	| customerBook |
	
	customerBook := CustomerBook new.
	self actionTakeLessThanCantTime: [customerBook addCustomerNamed: 'John Lennon'.]  cantTime: 50
	
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'ti 4/10/2023 21:41:10'!
test02RemovingCustomerShouldNotTakeMoreThan100Milliseconds

	| customerBook paulMcCartney |
	
	customerBook := CustomerBook new.
	paulMcCartney := 'Paul McCartney'.
	
	customerBook addCustomerNamed: paulMcCartney.
	self actionTakeLessThanCantTime: [customerBook removeCustomerNamed: paulMcCartney.]  cantTime: 100
	
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'ti 4/10/2023 21:55:02'!
test03CanNotAddACustomerWithEmptyName 

	| customerBook |
			
	customerBook := CustomerBook new.
	self invalidCodeThatMustFail: [ customerBook addCustomerNamed: ''.]  
	failCondition: Error 
	errorCode: [:anError | 
			self assert: anError messageText = CustomerBook customerCanNotBeEmptyErrorMessage.
			self assert: customerBook isEmpty ]
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'ti 4/10/2023 21:57:22'!
test04CanNotRemoveAnInvalidCustomer
	
	| customerBook johnLennon |
			
	customerBook := CustomerBook new.
	johnLennon := 'John Lennon'.
	customerBook addCustomerNamed: johnLennon.
	self invalidCodeThatMustFail: [customerBook removeCustomerNamed: 'Paul McCartney'.]
	 failCondition: NotFound 
	errorCode:  [:anError | 
			self assert: customerBook numberOfCustomers = 1.
			self assert: (customerBook includesCustomerNamed: johnLennon) ]

! !

!CustomerBookTest methodsFor: 'testing' stamp: 'TI 4/11/2023 19:47:20'!
test05SuspendingACustomerShouldNotRemoveItFromCustomerBook

	| customerBook |
	customerBook := self addAndSuspendCustomer: 'Paul McCartney'.
	self checkQuantityOfDifferentTypeOfCustomers: customerBook
	numberOfActiveCustomers: 0
	numberOfSuspendedCustomers: 1
	numberOfCustomers: 1.

	self assert: (customerBook includesCustomerNamed: 'Paul McCartney').
	

	
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'TI 4/11/2023 19:47:30'!
test06RemovingASuspendedCustomerShouldRemoveItFromCustomerBook

	| customerBook |
	customerBook := self addAndSuspendCustomer: 'Paul McCartney'.
	
	customerBook removeCustomerNamed: 'Paul McCartney'.
	
	self checkQuantityOfDifferentTypeOfCustomers: customerBook
	numberOfActiveCustomers: 0
	numberOfSuspendedCustomers: 0
	numberOfCustomers: 0.
	self deny: (customerBook includesCustomerNamed: 'Paul McCartney').


	
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'TI 4/11/2023 19:53:28'!
test07CanNotSuspendAnInvalidCustomer
	
	| customerBook |
			.
	customerBook := self addCustomer: 'John Lennon'.
	self invalidCodeThatMustFail: [customerBook suspendCustomerNamed: 'George Harrison'.]
	failCondition: CantSuspend 
	errorCode: [ :anError | 
			self assert: customerBook numberOfCustomers = 1.
			self assert: (customerBook includesCustomerNamed: 'John Lennon') ].
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'TI 4/11/2023 19:54:11'!
test08CanNotSuspendAnAlreadySuspendedCustomer
	
	| customerBook |
		
	customerBook := self addAndSuspendCustomer: 'John Lennon'.
	self invalidCodeThatMustFail: [customerBook suspendCustomerNamed: 'John Lennon'.]
	failCondition: CantSuspend  
	errorCode: [ :anError | 
			self assert: customerBook numberOfCustomers = 1.
			self assert: (customerBook includesCustomerNamed: 'John Lennon') ].
! !


!classDefinition: #CustomerBook category: 'CodigoRepetido-Ejercicio'!
Object subclass: #CustomerBook
	instanceVariableNames: 'suspended active'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!

!CustomerBook methodsFor: 'auxiliary' stamp: 'TI 4/13/2023 13:44:01'!
removeElementNamed: aName fromCollection: collection ifFoundCodeToExecute: code
 
	1 to: collection size do: 
	[ :index |
		aName = (collection at: index)
			ifTrue: [
				collection removeAt: index.
				code value .
			] 
	].! !


!CustomerBook methodsFor: 'testing' stamp: 'NR 4/3/2019 10:14:26'!
includesCustomerNamed: aName

	^(active includes: aName) or: [ suspended includes: aName ]! !

!CustomerBook methodsFor: 'testing' stamp: 'NR 4/3/2019 10:14:26'!
isEmpty
	
	^active isEmpty and: [ suspended isEmpty ]! !


!CustomerBook methodsFor: 'initialization' stamp: 'NR 9/17/2020 07:23:04'!
initialize

	active := OrderedCollection new.
	suspended:= OrderedCollection new.! !


!CustomerBook methodsFor: 'customer management' stamp: 'TI 4/11/2023 19:56:10'!
addCustomerNamed: aName

	aName isEmpty ifTrue: [ self signalCustomerNameCannotBeEmpty ].
	(self includesCustomerNamed: aName) ifTrue: [ self signalCustomerAlreadyExists ].
	
	active add: aName ! !

!CustomerBook methodsFor: 'customer management' stamp: 'NR 4/3/2019 10:14:26'!
numberOfActiveCustomers
	
	^active size! !

!CustomerBook methodsFor: 'customer management' stamp: 'NR 4/3/2019 10:14:26'!
numberOfCustomers
	
	^active size + suspended size! !

!CustomerBook methodsFor: 'customer management' stamp: 'NR 9/19/2018 17:36:09'!
numberOfSuspendedCustomers
	
	^suspended size! !

!CustomerBook methodsFor: 'customer management' stamp: 'TI 4/13/2023 13:44:56'!
removeCustomerNamed: aName 
	
 	self removeElementNamed: aName  fromCollection: active ifFoundCodeToExecute: [^aName].
	self removeElementNamed: aName  fromCollection: suspended ifFoundCodeToExecute: [^aName].
	
	^NotFound signal.
! !

!CustomerBook methodsFor: 'customer management' stamp: 'HernanWilkinson 7/6/2011 17:52'!
signalCustomerAlreadyExists 

	self error: self class customerAlreadyExistsErrorMessage! !

!CustomerBook methodsFor: 'customer management' stamp: 'HernanWilkinson 7/6/2011 17:51'!
signalCustomerNameCannotBeEmpty 

	self error: self class customerCanNotBeEmptyErrorMessage ! !

!CustomerBook methodsFor: 'customer management' stamp: 'NR 4/3/2019 10:14:26'!
suspendCustomerNamed: aName 
	
	(active includes: aName) ifFalse: [^CantSuspend signal].
	
	active remove: aName.
	
	suspended add: aName
! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CustomerBook class' category: 'CodigoRepetido-Ejercicio'!
CustomerBook class
	instanceVariableNames: ''!

!CustomerBook class methodsFor: 'error messages' stamp: 'NR 4/9/2023 22:25:52'!
customerAlreadyExistsErrorMessage

	^'Customer already exists!!!!!!'! !

!CustomerBook class methodsFor: 'error messages' stamp: 'NR 4/9/2023 22:25:56'!
customerCanNotBeEmptyErrorMessage

	^'Customer name cannot be empty!!!!!!'! !
